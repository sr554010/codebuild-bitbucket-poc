FROM python:3.9.1
RUN apt-get update
RUN apt-get -y install libleptonica-dev 
RUN apt-get -y install libtesseract-dev
RUN apt-get -y install tesseract-ocr
RUN apt-get -y install tesseract-ocr-all
ADD . /tesseract-python 
WORKDIR /tesseract-python
RUN pip install -r requirements.txt
CMD ["python","app.py"]